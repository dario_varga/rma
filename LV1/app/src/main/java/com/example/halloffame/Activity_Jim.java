package com.example.halloffame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class Activity_Jim extends AppCompatActivity {

    ImageView mImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__jim);


        mImage = findViewById(R.id.imageViewJim);
    }
}
